package no.noroff.SqlToRest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqlToRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SqlToRestApplication.class, args);
	}

}
