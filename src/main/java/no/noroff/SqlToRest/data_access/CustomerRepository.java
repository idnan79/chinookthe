package no.noroff.SqlToRest.data_access;

import no.noroff.SqlToRest.models.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

public interface CustomerRepository {
    public ArrayList<Customer> getAllCustomers();
    public Customer getCustomerById(int custId);
    public Customer getCustomerByName(String fName);
    public Boolean addCustomer(Customer customer);
    public Boolean updateCustomer(Customer customer);
    public Customer getCustomerBycountry(String country);

   public ArrayList<Customer> getAllCustomersCountry();

    public ArrayList<Customer> GetAllCustomersFromLimitOfset(int ofset, int limit);

    public ArrayList<Customer> getAllCustomersSales();

    public Customer getCustomerPopularGenre(int id);
}
