package no.noroff.SqlToRest.data_access;

import no.noroff.SqlToRest.logging.LogToConsole;
import no.noroff.SqlToRest.models.Customer;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/*
 This class serves as the encapsulation of all database interactions,
 it removes the implementation from the controllers to models - as they should only be responsible
 for handling user interactions and deciding what to do with it.
*/
@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final LogToConsole logger;

    // Setting up the connection object we need.
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public CustomerRepositoryImpl(LogToConsole logger) {
        this.logger = logger;
    }

    /*
     Need methods to serve the needs of the controller requests.
     Just mirror what your endpoints want.
    */

    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName, LastName,Company,Country,Phone,Email FROM customers");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                               resultSet.getString("Company"),
                                resultSet.getString("Country"),
                               resultSet.getString("Phone"),
                                resultSet.getString("Email")

                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return customers;
    }

    public ArrayList<Customer> getAllCustomersSales(){
        ArrayList<Customer> customers = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                           conn.prepareStatement("SELECT customers.CustomerId,customers.FirstName,customers.LastName,customers.Company,customers.Country,customers.Phone,customers.Email, SUM(invoices.Total) AS TotalItemsOrdered " +
                                   " FROM invoices, customers " +
                                   " Where invoices.CustomerId = customers.CustomerId Group by customers.FirstName, customers.LastName,customers.Company,customers.Country,customers.Phone,customers.Email ORDER BY TotalItemsOrdered DESC");

            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Company"),
                                resultSet.getString("Country"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")

                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return customers;
    }

    public ArrayList<Customer> getAllCustomersCountry(){
        ArrayList<Customer> customers = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName, LastName,Company,Country,Phone,Email FROM customers ORDER BY Country DESC ");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Company"),
                                resultSet.getString("Country"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")

                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return customers;
    }

    @Override
    public ArrayList<Customer> GetAllCustomersFromLimitOfset(int ofset, int limit) {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT  * FROM Customer  WHERE CustomerId BETWEEN ? AND ? ");
            preparedStatement.setInt(1, ofset + 1);
            preparedStatement.setInt(2, limit + ofset);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("Company"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
            logger.log("Select all customers successful");
        } catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());
            }
        }
        return customers;
    }

    @Override
    public Customer getCustomerById(int custId) {
        Customer customer = null;
        try {
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");
            // Make SQL query
            PreparedStatement preparedStatement =
                    //conn.prepareStatement("SELECT CustomerId,FirstName, LastName,Country,PostalCode,Phone,Email FROM customers WHERE CustomerId = ?");
                    conn.prepareStatement("SELECT CustomerId,FirstName, LastName,Company,Country,Phone,Email FROM Customers WHERE CustomerId = ?");
            preparedStatement.setInt(1, custId);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Company"),
                        resultSet.getString("Country"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
            logger.log("Select specific customer successful");
        } catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());
            }
        }
        return customer;
    }

    @Override
    public Customer getCustomerPopularGenre(int custId) {
        Customer customer = null;
        try {
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");
            // Make SQL query
            PreparedStatement preparedStatement =
                    //conn.prepareStatement("SELECT CustomerId,FirstName, LastName,Country,PostalCode,Phone,Email FROM customers WHERE CustomerId = ?");
                    conn.prepareStatement("SELECT TOP 1 customers.CustomerId,customers.FirstName,customers.LastName,customers.Company,customers.Country,customers.Phone,customers.Email,Genre.Name,COUNT(Genre.Name) AS GenreCount "+
                            "FROM customers "+
                            "INNER JOIN Invoice ON customers.CustomerId = invoices.CustomerId "+
                            "INNER JOIN InvoiceLine ON invoices.InvoiceId = InvoiceLine.InvoiceId "+
                            "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId "+
                            "INNER JOIN Genre ON Track.GenreId = Genre.GenreId "+
                            "WHERE customers.CustomerId = @CustomerId " +
                            "GROUP BY Genre.Name , customers.CustomerId,customers.FirstName,customers.LastName,customers.Company,customers.Country,customers.Phone,customers.Email "+
                            "ORDER BY GenreCount DESC"
                    );
            preparedStatement.setInt(1, custId);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Company"),
                        resultSet.getString("Country"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
            logger.log("Select specific customer successful");
        } catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());
            }
        }
        return customer;
    }

    @Override
    public Customer getCustomerByName(String fName) {
        Customer customer = null;
        try {
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");
            // Make SQL query
            PreparedStatement preparedStatement =
                    //conn.prepareStatement("SELECT CustomerId,FirstName, LastName,Country,PostalCode,Phone,Email FROM customers WHERE CustomerId = ?");
                    conn.prepareStatement("SELECT CustomerId,FirstName, LastName,Country,Company,Phone,Email FROM Customers WHERE FirstName = ?");
            preparedStatement.setString(1, fName);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Company"),
                        resultSet.getString("Country"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
            logger.log("Select specific customer successful by First name");
        } catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());
            }
        }
        return customer;
    }

    @Override
    public Customer getCustomerBycountry(String country) {
        Customer customer = null;
        try {
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");
            // Make SQL query
            PreparedStatement preparedStatement =
                    //conn.prepareStatement("SELECT CustomerId,FirstName, LastName,Country,PostalCode,Phone,Email FROM customers WHERE CustomerId = ?");
                    conn.prepareStatement("SELECT CustomerId,FirstName, LastName,Company,Country,Phone,Email FROM Customers WHERE Country = ? ");
            preparedStatement.setString(1, country);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Company"),
                        resultSet.getString("Country"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
            logger.log("Select specific customer successful by First name");
        } catch (Exception exception) {
            logger.log(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                logger.log(exception.toString());
            }
        }
        return customer;
    }


    public Boolean addCustomer(Customer customer){
       Boolean success = false;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
        PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customers(FirstName,LastName,Company,Phone,Email) VALUES(?,?,?,?,?)");

            preparedStatement.setString(1,customer.getFirstName());
            preparedStatement.setString(2,customer.getLastName());
            preparedStatement.setString(3,customer.getCompany());
            preparedStatement.setString(4,customer.getPhone());
            preparedStatement.setString(5,customer.getEmail());
            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Add customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return success;
    }

    public Boolean updateCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE customers SET CustomerId = ?, FirstName = ?, LastName = ?, Phone = ? ,Email = ? WHERE CustomerId=?");
            preparedStatement.setInt(1,customer.getCustomerId());
            preparedStatement.setString(2,customer.getFirstName());
            preparedStatement.setString(3,customer.getLastName());
            preparedStatement.setString(4,customer.getEmail());
            preparedStatement.setString(5,customer.getPhone());
            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Update customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
       return success;
    }
}
