package no.noroff.SqlToRest.controllers;

import no.noroff.SqlToRest.data_access.CustomerRepository;
import no.noroff.SqlToRest.data_access.CustomerRepositoryImpl;
import no.noroff.SqlToRest.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {
    // Configure some endpoints to manage crud
    private final CustomerRepository customerRepository;

    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    /*
     This first one just returns all the customers in the database
     it will return a CustomerShort object.
    */
    @RequestMapping(value="/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }

    /*
     This first one just returns all the customers in the database
     it will return a CustomerShort object.
    */
    @RequestMapping(value="/api/invoices/total", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomersSales(){
        return customerRepository.getAllCustomersSales();
    }

    /*
      This first one just returns all the customers in the database
      it will return a CustomerShort object.
     */
    @RequestMapping(value="/api/customers/Country", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomersCountry()
    {
        return customerRepository.getAllCustomersCountry();
    }

    /*
          This first one just returns all the customers in the database
          it will return a CustomerShort object.
         */
    @RequestMapping(value="/api/customers/limit", method = RequestMethod.GET)
    public ArrayList<Customer> GetAllCustomersFromLimitOfset(@PathVariable int Ofset, @PathVariable int Limit){
        return customerRepository.GetAllCustomersFromLimitOfset(Ofset, Limit);
    }

    /*
     This returns a specific customer, based on a given Id.
     We use a query string here to extract the Id.
     E.g. api/customer?id=ALFKI
    */

    @RequestMapping(value = "api/customers/id/{id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable int id){
        return customerRepository.getCustomerById(id);
    }

    /*
     This returns a specific customer, based on a given Id.
     We use a query string here to extract the Id.
     E.g. api/customer?id=ALFKI
    */

    @RequestMapping(value = "api/customers/popgen/{id}", method = RequestMethod.GET)
    public Customer getCustomerPopularGenre(@PathVariable int id){
        return customerRepository.getCustomerPopularGenre(id);
    }

    /*
     This returns a specific customer, based on a given firstName.
     We use a query string here to extract the FirstName.
     E.g. api/customer?FirstName=ALFKI
    */

    @RequestMapping(value = "api/customers/fname/{fName}", method = RequestMethod.GET)
    public Customer getCustomerByName(@PathVariable String fName){
        return customerRepository.getCustomerByName(fName);
    }

    /*
     This returns a specific customer, based on a given firstName.
     We use a query string here to extract the FirstName.
     E.g. api/customer?FirstName=ALFKI
    */

    @RequestMapping(value = "api/customers/cname/{country}", method = RequestMethod.GET)
    public Customer getCustomerByCountry(@PathVariable String country){
        return customerRepository.getCustomerBycountry(country);
    }


    /*
     This adds a new customer.
     It takes the new customer from the body of the request.
    */
    @RequestMapping(value = "api/customers/add", method = RequestMethod.POST)
    public Boolean addNewCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }
    /*
     This updates an existing customer.
     It takes the new customer data from the body of the request.
     As well as taking the Id of the customer from the path variables, this is
     to do a check if the body matches the id. Just a layer of saftey.
    */
    @RequestMapping(value = "api/customers/id/{id}", method = RequestMethod.PUT)
    public Boolean updateExistingCustomer(@PathVariable int id, @RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }


}
