# SQL to REST 

> A simple project for demonstrating building a REST API using an SQLite3 database. 

## Maintainers 

[Nicholas Lennox (@NicholasLennox)](https://gitlab.com/NicholasLennox)

## License

---
Copyright 2020, Nicholas Lennox ([@NicholasLennox](https://gitlab.com/NicholasLennox))